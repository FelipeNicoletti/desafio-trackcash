const express = require('express');
const app = express();
const cors = require('cors');
const User = require('./model/User');

app.use(cors());
app.use(express.json())

app.post('/user',(req,res)=>{
    var {nome,email,doc,telefone,password} = req.body;
    var userObject = new User(nome,email,doc,telefone,password);
    var response = userObject.validation(res);

    return res.json(response);
    
})

app.post('/auth',(req,res)=>{
    var {email,senha} = req.body;

    var userObject = new User(null,email,null,null,senha);
    userObject.validation(res);
})

app.post('/recovery',(req,res)=>{
    var {email} = req.body;

    var userObject = new User(null,email,null,null,null);
    userObject.recoveryPassword(res);

    })

app.post('/redefinition',(req,res)=>{
    var {new_password} = req.body;
    var {email} = req.query;

    var userObject = new User(null,email,null,null,new_password);
    userObject.updatePassword(res)

})

app.listen(3000);