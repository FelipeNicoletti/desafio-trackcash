const connection =require('../services/mysql');
const nodemailer = require('nodemailer');

class User{
    user;
    email;
    cpf_cnpj;
    telefone;
    senha;
    queryStorager;

    constructor(user,email,cpf_cnpj,telefone,senha) {
        this.user = user;
        this.email = email;
        this.cpf_cnpj = cpf_cnpj;
        this.telefone = telefone;
        this.senha = senha;
        this.queryStorager = null;
    }

    setStorage(value){
      this.queryStorager = value;
    }

    insert(){
        var queryString = `INSERT INTO user(cpf_cnpj,nome,email,telefone,senha) VALUES("${this.cpf_cnpj}","${this.user}","${this.email}","${this.telefone}","${this.senha}")`;
        var result = connection.query(queryString)
  
        if(!result.err){
          return {"transaction":true};
        }
        return {"transaction":false};
        
    }

    recoveryPassword(res){
        var queryString = `SELECT email FROM user WHERE email = "${this.email}" `;

        connection.query(queryString,(err,result)=>{
            if(err) throw err;
            const {email} = result[0];

            if(email != undefined){
                var transporter = nodemailer.createTransport({
                  service: 'gmail',
                  auth: {
                    user: 'felipenicolettirmario@gmail.com',
                    pass: '*******'
                  }
                });
                
                var mailOptions = {
                  from: 'felipenicolettirmario@gmail.com',
                  to: email,
                  subject: 'Password recovery',
                  text: 'http://localhost/teste?teste=true'
                };
      
                transporter.sendMail(mailOptions, function(error, info){
                  if (error) {
                    throw error;
                  } else {
                    console.log('Email sent: ' + info.response);
                  }
                });
                return res.json({"sent":true})
            }
            return res.json({"sent":false})
        })

        //var service = emailQuery.split('@')[1].split('.')[0]


    }

    updatePassword(res){

        var queryString = `UPDATE user SET  senha = "${this.senha}" WHERE email="${this.email}"`;

        connection.query(queryString,(err)=>{
          if (err) return res.json({"updated":false});
          return res.json({"updated":true})
        })

    }

    
    validation(res){

        var queryString = `SELECT (senha) FROM user WHERE email = "${this.email}" and senha = "${this.senha}"`;

        connection.query(queryString,function (err, result, fields) {
          if (err) throw err;
          
          if(result.lenght>=0){
            return res.json({"validation":true})
          }
          return res.json({"validation":false})
          
        })
        
    }


}

module.exports = User;